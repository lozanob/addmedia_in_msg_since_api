
	/**
 * @file
 * Drupal File plugin.
 *
 * @ignore
 */
 CKEDITOR.DIALOG_RESIZE_NONE = 0;
 CKEDITOR.DIALOG_RESIZE_WIDTH = 1;
 CKEDITOR.DIALOG_RESIZE_HEIGHT = 2;
 CKEDITOR.DIALOG_RESIZE_BOTH = 3;
 


(function ($, CKEDITOR) {
	'use strict';

	CKEDITOR.dialog.add( 'abbrDialog', function( editor ){

        
          // taille boite dialog 
          var minWidth  = (jQuery(window).width() * 80 /100);  
          var minHeight =   (jQuery(window).height() * 50 /100); 



       return {
        title: 'Media add',
        minWidth: minWidth,
        minHeight: minHeight,
        contents: [
            {
                id: 'tab-basic',
                label: 'Basic Settings',
                elements: [{
                id:'htmlMedia',
                type: 'html',
                html:'<div id="mediaOps"  style="display:flex; flex-wrap:wrap;"></div>',

                }],
            }
        ],

         onShow: function(){    

              opsomaiApi();

        },
        onOk: function() {


          jQuery("div#mediaOps div.media-div-Per").each(function(index) {

              if(jQuery(this).hasClass("checked") ){

                 var img = editor.document.createElement('img');
                 var src =  jQuery(this).find("div.media-div-img img").attr("src");
                 var width = jQuery(this).find("div.media-div-img img").css("width") ; 
                 var height = jQuery(this).find("div.media-div-img img").css("height") ; 

                 //img.style.cssText= "margin-right : 3px";  
                 img.setAttribute('src', src);
                 img.setAttribute('width', width);
                 img.setAttribute('height', height);
                 editor.insertElement(img);

              }

          });
                
                cleanCheckInput(); 

		},

  };


function  MediaApi(url,param){

          
                  if( url  === undefined  || url === null)
                       url = 'http://ch-ops-dev/int/minagri/www/service.php?urlaction=recherche'; 
                  
  
                  var request=jQuery.ajax({
                          type: "POST",
                          url: url,
                          data: "api_key=E2D6139Z325a49Eh87GL8396",
                          dataType: "xml",
                          async: false,

                          success: function(xml) {

						  
                             var nb_lignes  = Math.round( (parseInt( jQuery(jQuery(xml)).find("rows").find('row').length)/parseInt( jQuery(jQuery(xml)).find("data").find('nb_pages').text())) );   // nb_element / nb_pages 
                             var nb_element_x_lignes = Math.round((parseInt( jQuery(jQuery(xml)).find("rows").find('row').length) % parseInt( jQuery(jQuery(xml)).find("data").find('nb_pages').text())));    // nb_element * nb_pages
                             var cpt = 1; 
                             var row = ''; 

                              // Gestion des errors/connection api 
                              try{

                                if( jQuery(jQuery(xml)).find("Status").is(':empty') ) throw "is Empty";
                                if(isNaN(nb_lignes) || isNaN(nb_element_x_lignes)  ) throw "not a number";
                                if(cpt !=  1) throw "too high";
                              
                              }catch(err) {

                                console.log("Error : " , err  ); 

                                alert("Error connection"); 

                             }


                             //affichage window 
                              jQuery(jQuery(xml)).find("rows").each(function(){

                                  for(var i = 1; i <= nb_element_x_lignes; i++) {
                                              //var max = Math.floor(Math.random() * Math.floor(nb_element); 
                                               jQuery(this).find('row')[i].remove(); // ici 
                                  }

                                  jQuery(this).find("row").each(function(){
                                              if(cpt <= nb_lignes){
                                                      row+='<div class="media-div-Per" style="margin:auto;" id="div-'+jQuery(this).attr("rang")+'">';
                                                      row+= '<div id="div-img-'+jQuery(this).attr("rang")+'" class="media-div-img"><img  id="img-'+jQuery(this).attr("rang")+'"  style="object-fit:contain" src="'+jQuery(this).find("vignette").text()+'"></div>'; 
                                                      row+='<div class="media-div-info"><label style="display:none;">Ref: '+jQuery(this).find("reference").text()+'  </label> <input type="checkbox" name="'+jQuery(this).find("reference").text()+'"></div>'; 
                                                      row+='</div>'; 
                                               if(cpt === nb_lignes){
                                                      jQuery('div#mediaOps').append('<div class="bloque" style="width:100%; display:flex">'+row+'</div>');

                                                     cpt=0; 
                                                     row=''; 
                                                  }
                                              }

                                              cpt ++; 
                                      }); 
                              }); 

                                cleanCheckInput(); 


                          },
                          complete:function(xml) {
 

                                  //appelle de function 
                                    hoverOpsomaiApi("div#mediaOps div.media-div-Per div.media-div-img"); 
                                    cssApi();

                                    //select media  
                                    jQuery("div#mediaOps div.media-div-Per").click(function() {

                                                        if( !jQuery(this).hasClass("checked") ){
                                                            jQuery(this).addClass("checked");
                                                            jQuery( this).find("div input").attr('checked','checked'); 
                                                        }else{
                                                            jQuery(this).removeClass("checked");
                                                            jQuery( this).find("div input").attr('checked', false ); 
                                                        }
                                    });



                          }, 
                  });

                // conection status connection 
              request.fail(function( jqXHR, textStatus ) {
                       alert( "Request failed: " + textStatus );
                       jQuery('div#mediaOps').html("<p style='color:red;margin:auto;font-size:24px;'>Request server failed</p>"); 
               });
}



      // css  boite dialoge 
      function cssApi(){
              jQuery('div#mediaOps').css('background-image', 'url(" ")');
              jQuery("div#mediaOps div.bloque").css({"background-color":"#CCC"});
              jQuery("div#mediaOps div.bloque div.media-div-img img").css({"width":"50px","height":"50px"}); 
              jQuery("div#mediaOps div.bloque div.media-div-info").css({"display":"flex","justify-content":"center"}); 
              jQuery("div#mediaOps div.bloque div.media-div-Per").css({"border":"1px solid white","margin-bottom":"8px" });
      }// fin function 



      function cleanCheckInput(){
		  
              jQuery("div#mediaOps div.media-div-Per").each(function(index) {
                      jQuery(this).removeClass("checked");
                      jQuery(this).find("div input").attr('checked', false ); 
              });
      }



        function hoverOpsomaiApi(select, color=''){

            if( ! (typeof select === "string")  ||  select === undefined   ||  select === null){
                           return alert("Selecter Jgquery is not type String ");  
            }

            add_library_Js(cdn_VidoJs_(), 'div#mediaOps div.media-div-Per div.media-div-img'); 

           jQuery(select).hover(function(e){

                    jQuery(this).css("background", "rgba(77,77,77,0.5)");   //add  Effet default 

                    var element = jQuery(this).find("img").clone().css({"min-width":"100%","min-height":"80%"}); 
                    var idDisplay =  "D"+jQuery(this).attr('id');

                    var elementTest = 'video'; 
                    var srcvideoTest = 'url....';  //ok 

                    // for image !! 
                 if (elementTest == 'img'){
                        display(idDisplay,e, element); 
                  }

                  //for Video 
                 if (elementTest == 'video'){
                    display(idDisplay , e, null);  // creation div DOM in none 
                    callHoverPlayer(idDisplay , srcvideoTest); //creation balise video and js video  and add in div in none 
                  }

          			// time for  show element 
          			var timeout = setTimeout(function(){
                  			jQuery("body").find('div.draggable').css("display","block");  // ouverture de div nonne 
            		 }, 2000);


          }, function(){
                  jQuery("html body").find('div.draggable').remove();
                  jQuery(this).css("background", "rgba(0,0,0,0)");   
                  jQuery('html head .cdnJs').remove(); 
          });

        }// fin function 



        /** work with plugin video js  https://docs.videojs.com/ | DOC 
          * return  templaite video
           * selectVideo  selected windown in video | string | Requerid  
          * src:  path video | string  | Requerid 
        **/ 
	    function callHoverPlayer(selectVideo , src ){

	    		var j = selectVideo; 
      
               	if(selectVideo === undefined || selectVideo === '') throw new Error("Parameter selectVideo  is  Requerid !"); 

        	    var video = jQuery('<video />', { id:'my-video', "class":'video-js'}).appendTo('#'+j);

                if(src === undefined || src === null)throw new Error("Parameter src is  Requerid !"); 

  				var scriptObj = "var video=videojs('my-video',"+JSON.stringify({controls: true, loop:true, autoplay:'muted',width:'250px',height:'150px',
  					sources:[{src: src, type:'video/mp4'}]})+");"; 

               jQuery('<script class="cdnJs">'+ scriptObj +'</' + 'script>').appendTo("html head"); 
        }





        /** work with plugin video js  https://docs.videojs.com/ | DOC 
          * return  Display paramettre for your position. 
          * id:  id display | string  | Requerid 
          * positionDisplay  (pageX|pageY) get in action  hover .. click .. | string | Requerid  
          * elementIn:  DOM element including in Display | string  | Optionel 
        **/ 
        function display(id , positionDisplay ,  elementIn){

                  if(id === undefined || id === '')throw new Error("Parameter id  is  Requerid !"); 

                  if(positionDisplay === undefined || positionDisplay === '')throw new Error("Parameter position  is  Requerid !"); 

                   var myDiv = jQuery('<div/>', {id: id,"class":'draggable'}).appendTo('body');

                    //css div windown // position dynamique 
                    jQuery("div#"+id).css({"position":"absolute","width":"250px","height":"150px","z-index":"30000","display":"none",
                                                       "left": positionDisplay.pageX,"top":positionDisplay.pageY,"background":"rgba(77,77,77,0.5)"});    

                    	 //console.log('id ' , elementIn ) ; 
                       if(elementIn != undefined || elementIn != '' || elementIn != null){
                          	jQuery("div#"+id).append(elementIn); 
                       }


                       return myDiv; 
        }



        // Doc https://docs.videojs.com/
        function cdn_VidoJs_(){
    		var CdnVidoJs='<link class="cdnJs" href="https://vjs.zencdn.net/7.6.6/video-js.css" rel="stylesheet" />'; 
 			CdnVidoJs+='<script class="cdnJs" src="https://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>'; 
    		CdnVidoJs+='<script class="cdnJs" src="https://vjs.zencdn.net/7.6.6/video.js"></script>'; 
    		CdnVidoJs+='<script class="cdnJs" src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>'; 
        	return CdnVidoJs;
        }



        /*return library js | ( add and remove ) script in page 
        *lib : library for add   | string 
        * var select : selector jQUERY for include library| string  
        * add classname  in script cdnJs  | Obligation  <script class='cdnJs... 
        */
        function add_library_Js(lib = '' , select = ''){


          if(lib == undefined || lib == null)var lib = ''; 
           
          if( ! (typeof lib === "string"))return alert("Lib add in libraryJs() => Is not a String  <string></string>");  
          
          
          var newLib = '';   
          if (  (lib.match("^<script ") ) || (lib.match("^<link ") ) || (lib.match("<script>$"))){
                newLib=lib; 
          }else {
              newLib = undefined;
          }

          var addlib  = (newLib === undefined || newLib === '') ? '' : newLib; 

          if( ! (typeof select === "string"))return alert("Selecter Jgquery is not type String");  
          

                jQuery(select).hover(function(e){
                        jQuery('html head').append(addlib); 
                  }, function(){
                      // className for deve rendre dynamique 
                      jQuery('html head .cdnJs').remove(); 
                }); 

              return addlib; 
        }






});  // fin plugin crediteur 


})(jQuery, CKEDITOR);