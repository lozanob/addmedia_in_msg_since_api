
/**
 * @file
 * Drupal File plugin.
 *
 * @ignore
 */
(function ($, CKEDITOR) {
	'use strict';

	CKEDITOR.plugins.add('abbr', {
	    icons: 'example',
	    init: function( editor ) {

	   		// creation d'une commande 
	   		editor.addCommand( 'abbr', new CKEDITOR.dialogCommand('abbrDialog') );

	   		// gerer l'affichage des images avec css grace à spaceId 
	   		// une simple button
		    editor.ui.addButton('abbr', {
		    label: 'Media',
		    command: 'abbr',
		    toolbar: 'insert',
		    icon: this.path + 'example.png'
			});

	    	// path de la logique de mon plugin js 
	        CKEDITOR.dialog.add('abbrDialog', this.path + 'boiteDialog.js');

	    } // fin  function init 


	});


})(jQuery, CKEDITOR);