<!DOCTYPE html>
<html>
<head>
            <meta charset="utf-8">
            <title>CKEditor</title>
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
			<script src="//cdn.ckeditor.com/4.5.6/standard/ckeditor.js"></script>
            <script src="plugin.js"></script>
            <script src="boiteDialog.js"></script>
</head>
<body>


		  <textarea name="editor1"></textarea>
          <script type="text/javascript">


			$(document).ready(function(){

					// charger mon plugin 
					CKEDITOR.config.extraPlugins = 'abbr'; // initialisation 
					CKEDITOR.config.allowedContent  = true; // 
					CKEDITOR.env.isCompatible = true;
			
					CKEDITOR.plugins.addExternal('abbr', '', 'plugin.js' );
					CKEDITOR.replace('editor1', {extraPlugins: 'abbr'});
					CKEDITOR.plugins.addExternal('abbrDialog', '', 'boiteDialog.js' );

						// position facultatif 
						// This could position the button at the beginning of the "insert" group.
						//toolbar: 'insert,0'
						// This could position the button at the end of the "insert" group.
					    //toolbar: 'insert,100'


			}); 




			</script>



</body>
</html>